 jQuery(document).ready(function(jQuery) {
      $('.fadeOut').owlCarousel({
        items: 1,
        animateOut: 'fadeOut',
        loop: true,
        margin: 10,
      });
      jQuery('.customMain').owlCarousel({
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        items: 1,
        margin: 30,
        stagePadding: 30,
        smartSpeed: 450
      });
    });

/*Owl-2 */
jQuery(document).ready(function() {
  jQuery('.owl-carousel').owlCarousel({
    loop: false,
    margin: 15,
      nav: true,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
       
      },
      600: {
        items: 3,
       
      },
      1000: {
        items: 4,
        
       
      }
    }
  })
})

/*  */
 jQuery(function(){
      SyntaxHighlighter.all();
    });
    jQuery(window).load(function(){
      jQuery('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 160,
        itemMargin: 5,
        asNavFor: '#slider'
      });

      jQuery('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel",
        start: function(slider){
          jQuery('body').removeClass('loading');
        }
      });
    });
    
